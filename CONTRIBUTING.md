**Language/ Sprache:**<br/>
[Deutsch/ German](#Deutsch)
[English/ Englisch](#English)

# Deutsch
<!-- Guide: https://mozillascience.github.io/working-open-workshop/contributing/ -->
## Code Style Leitfaden
In diesem Projekt wir der Code-Style von [Google](https://google.github.io/styleguide/cppguide.html) verwendet.

## Tags/ Version
Ein Tag sollte immer so aufgebaut sein:    
**v[VERSION](#Versions Nummer)**    
Beispiel:
```
v1.4.2-RC
```

## Versions Nummer
Die Versionsnummer ist so aufgebaut:    
```
v2.3.5-ALPHA
 | | | └────── evtl. Addition
 │ │ └──────── Patch Level     
 │ └────────── Minor Nummer 
 └──────────── Major Nummer
```

### Major
Indiziert extrem wichtige Änderungen am Programm - z.B. wenn das Programm komplett neu geschrieben wurde (z.B. GIMP 2. x nach Version 1. x) oder wenn die Schnittstellenkompatibilität für Bibliotheken nicht aufrecht erhalten werden kann.

### Minor
Bezieht sich auf die funktionale Erweiterung des Programms.    
z. B. Wenn eine neue Funktion hinnzugefügt wurde.

### Patch Level
Beinhaltet Fehlerbehebungen.

### Build
Gibt den Fortschritt der Entwicklungsarbeit in Einzelschritten an, d. h. beginnend mit 0001 wird er um eins erhöht, beginnend mit jeder Kompilierung des Codes.  Version 5.0.0.0.0-3242 steht für das 3242. Kompilierungsprodukt einer Software.  Wenn Versionskontrollsysteme verwendet werden, wird anstelle der Build-Nummer häufig eine Nummer verwendet, die die Quellen für die Kompilierung innerhalb des Versionskontrollsystems eindeutig identifiziert.  Dies erleichtert das Auffinden der entsprechenden Quellen im Fehlerfall.
Die Buildnummer gibt auch  wieder wie viele Commit vergangen sind seit dem letzten Tag. Wird automatisch von dem Build System gesetzt.

### Revision
Die Revisions Nummer gibt die Zahl aller exesiterenden Commits im aktuellen Branch wieder.  Wird automatisch von dem Build System gesetzt.

### Addition
Bitte verwenden Sie die folgenden Zusätze:    
**ALPHA**: während der Softwareentwicklung, sehr frühes Stadium     
**BETA**: zum Testen vorgesehen, begrenzte Anzahl von Benutzern    
**RC**: Verlagskandidat, endgültige Testversion    
Bitte verwenden Sie keinen Zusatz für (Final) Release oder Patch!

## Veränderung wichtiger Dateien
Sollten wichtige Dateien (wie z. B. [CONTRIBUTING.md][], [README.md][], [LICENSE][], [CHANGELOG.md][], etc.) geändert werden, **muss** der Commit signiert werden!

## Neue Version (Release)
Bitte lesen Sie zuerst über [Versionsnummer](#Versions Nummer)<br/>
Es wird mit dem Git Flow Modell gearbeitet.<br/>
Weitere Informationen: [Git Flow Branching Model][]<br/>

## Pull Request
Damit ein Pull Request angenomment wird muss sichergestellt sein dass der bearbeitete Programm-Code gut dokumentiert ist!<br/>
Zur verbesserung der Lesbarkeit der Commit sollten diese mit folgendem Schema "erstellt" werden (Title):<br/>
Ein <bold>*</bold> für eine Änderung. _(Beispiel: ```* Edited README```)_<br/>
Ein <bold>+</bold> für eine neue Datei. _(Beispiel: ```+ Added File 'src/health.cpp'```)_<br/>
Ein <bold>-</bold> für das entfernen einer Datei. _(Beispiel: ```- Removed File 'include/health_old.h'```)_<br/>
Ein <bold>!</bold> für eine Wichtige Information. _(Beispiel: ```! Fixed Merge Request Error```)_<br/>
<br/>
Der "Inhalt" des Commits sollte dann mit dem gleichen Schema fortgeführt werden, nur viel Detail reicher.

<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->

# English
## Code Style Guide
In this project the code style of [Google](https://google.github.io/styleguide/cppguide.html) is used.

## Tags/ Version
A label should always be structured according to the following scheme:      
**v[VERSION](#Version Number)** 
Example:
```
v1.4.2-RC
```

## Version Number
```
v2.3.5-ALPHA
 | | | └────── evtl. Addition
 │ │ └──────── Patch Level     
 │ └────────── Minornumber 
 └──────────── Majornumber
```

### Major
Indexes extremely significant changes to the program - for example, if the program has been completely rewritten (for example, GIMP 2. x after version 1. x) or if interface compatibility cannot be maintained for libraries.

### Minor
Refers to the functional extension of the program.    
e. g. When a new function has been added.

### Patch Level
Contains bug fixes.

### Build
Indicates the progress of the development work in single steps, i. e. starting with 0001, it is incremented by one, starting with every compilation of the code.  Version 5.0.0.0-3242 stands for the 3242th compilation product of a software.  If version control systems are used, instead of the build number, a number is often used which uniquely identifies the sources for compilation within the version control system.  This makes it easier to find the corresponding sources in case of an error.
The build number also shows how many commits have passed since the last day. Set automatically by the build system.

### Revision
The revision number represents the number of all existing commits in the current branch. Set automatically by the build system.

### Addition
Please use the following Additions:    
**ALPHA**: during software development, very early stage     
**BETA**:  intended for testing, limited number of users    
**RC**:    Publishing candidate, final test version    
Please do not use an Addition for (Final) Release or Patch!

## Change important files
If important files (such as [CONTRIBUTING.md][],[README.md][],[LICENSE][],[CHANGELOG.md][], etc.) are changed **the commit must be signed**!

## Make a Release
Please read first about [Version Number](#Version Number).<br/>
The Git Flow model is used.<br/>
Further information: [Git Flow Branching Model][]<br/>

## Pull Request
For a pull request to be accepted, it must be ensured that the processed program code is well documented!<br/>
To improve the readability of the commit, they should be "created" with the following scheme (title):<br/>
A <bold>*</bold> for a change. (Example: ````* Edited README````)_<br/>
A <bold>+</bold> for a new file. (Example: ````+ Added File `'src/health.cpp'````)_<br/>
A <bold>-</bold> for removing a file. (Example: ```- Removed File 'include/health_old.h'````)_<br/>
A <bold>!</bold> for important information. (Example: ```! Fixed Merge Request Error````)_<br/>
<br/>
The "content" of the commit should then be continued with the same scheme, but much more Details.

<!-- ============ -->
[Git Flow Branching Model]: http://nvie.com/posts/a-successful-git-branching-model/
[CONTRIBUTING.md]: CONTRIBUTING.md
[README.md]: README.md
[LICENSE]: LICENSE
[CHANGELOG.md]: CHANGELOG.md